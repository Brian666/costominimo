﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace CostoMinimo
{
    class Cmin
    {
        #region variables
        int[,] matriz = new int[10000, 1000];
        int fila, columna, i, j,menor;
            #endregion variables
        #region generar
        public void practica (TextBox textBox1,TextBox textBox2)
            {
            fila =int.Parse(textBox1.Text);
            columna =int.Parse( textBox2.Text);
            for (i=0;i<fila;i++)
            {
                for(j=0;j<columna;j++)
                {
                    matriz[i, j] = int.Parse(Interaction.InputBox("Agregar elemento", "Costo minimo", "", fila, columna));
                }
            }
            }
        #endregion generar
        #region mostrar
        public void mostrar (DataGridView tabla)
        {
            tabla.ColumnCount = columna;
            tabla.RowCount = fila;
            
            for (i=0;i<=fila-1;i++)
            {
                for (j = 0; j <= columna-1; j++)
                {
                    tabla.Rows[i].Cells[j].Value = matriz[i, j].ToString();
                }
            }
            menor = matriz[0, 0];
            for (i = 0; i <= fila - 1; i++)
            {
                for (j = 0; j <= columna - 1; j++)
                {
                   if(matriz[i,j]<menor)
                    {
                        menor = matriz[i, j];
                    }
                }
            }
            
        }
        #endregion mostrar
        #region menor
        public void menorque(TextBox textBox8)
        {
            menor = matriz[0, 0];
            for (i = 0; i <= fila - 1; i++)
            {
                for (j = 0; j <= columna - 1; j++)
                {
                    if (matriz[i, j] < menor)
                    {
                        menor = matriz[i, j];
                    }
                }
            }
            textBox8.Text = menor.ToString();
        }
        #endregion menor
    }
}
